(** Arbres binaires de recherche

  Définit le type des arbres binaires ainsi que les fonctions de
  recherche, insertion et suppression. Ces fonctions sont démontrées
  correctes et complètes vis-à-vis de leur spécification.

  Auteurs :

  - Rémi Ceres <remi.ceres@etu.umontpellier.fr>
  - Mattéo Delabre <matteo.delabre@etu.umontpellier.fr>

  Testé avec Coq 8.9.0 *)

(** * Imports *)

Require Export Omega.
Require Export FunInd.

(** * Définition du type arbre binaire

  Un arbre binaire peut être soit :

  - un arbre vide (bt_empty)
  - un nœud (bt_node) contenant :
    - une valeur (x) ;
    - un sous-arbre binaire gauche (tleft) ;
    - un sous-arbre binaire droit (tright). *)

Inductive BinaryTree : Set :=
| bt_empty
| bt_node : nat -> BinaryTree -> BinaryTree -> BinaryTree.

Notation "[ ]" := bt_empty.
Notation "[ x , tleft , tright ]" := (bt_node x tleft tright).
Notation "[ x ]" := (bt_node x bt_empty bt_empty).

(** Quelques exemples d’arbres binaires :

  - le singleton : *)
Example ex_bt_single := [1].

(** - un arbre plus complexe : *)
Example ex_bt_small := [2, [4], []].

(** - un arbre avec les mêmes valeurs mais ordonnées différemment *)
Example ex_bt_other := [4, [], [2]].

(** - un sur-arbre des deux précédents : *)
Example ex_bt_bigger := [8, [2, [4], []], [1]].

(** * Prédicats sur les arbres binaires *)

(** ** Appartenance à un arbre

  Une valeur n appartient à un arbre t si :

  - n se trouve à la racine de l’arbre ;
  - n appartient au sous-arbre gauche ;
  - n appartient au sous-arbre droit. *)

Reserved Notation "n ∊ t" (at level 70, no associativity).

Inductive InTree : nat -> BinaryTree -> Prop :=
| in_tree_node : forall (n : nat) (tleft tright : BinaryTree),
    n ∊ [n, tleft, tright]
| in_tree_left : forall (x n : nat), forall (tleft tright : BinaryTree),
    n ∊ tleft
    -> n ∊ [x, tleft, tright]
| in_tree_right : forall (x n : nat), forall (tleft tright : BinaryTree),
    n ∊ tright
    -> n ∊ [x, tleft, tright]
where "n ∊ t" := (InTree n t).

(** Lemme : si un élément appartient à l’un des sous-arbres, il
    appartient à l’arbre *)

Lemma in_tree_hereditary : forall (x n : nat) (tl tr : BinaryTree),
  n ∊ tl \/ n ∊ tr
  -> n ∊ [x, tl, tr].
Proof.
  intros x n tl tr.
  intros n_in_tl_or_tr.
  inversion n_in_tl_or_tr.
    (* n ∊ tl *)
    apply in_tree_left.
    exact H.

    (* n ∊ tr *)
    apply in_tree_right.
    exact H.
Qed.

(** ** Majorant d’un arbre binaire

  Une valeur n majore un arbre binaire si elle est strictement supérieure
  à chacun des éléments de l’arbre. *)

Reserved Notation "T << n" (at level 70, no associativity).

Inductive BinaryTreeLt : BinaryTree -> nat -> Prop :=
| bt_lt : forall (t : BinaryTree) (n : nat),
  (forall x : nat, x ∊ t -> x < n)
  -> t << n
where "T << n" := (BinaryTreeLt T n).

(** Tactique pour montrer qu’une valeur majore un arbre binaire *)

Ltac binary_tree_lt :=
  repeat
    match goal with
    | [ H : ?x ∊ ?t |- _ ] => inversion_clear H
    | [ |- ?T << ?n] => apply bt_lt; intros
    | [ |- ?n0 < ?n] => omega
    end.

Goal [] << 0.
Proof.
  binary_tree_lt.
Qed.

Goal ex_bt_single << 5.
Proof.
  binary_tree_lt.
Qed.

Goal ex_bt_small << 5.
Proof.
  binary_tree_lt.
Qed.

(** Théorème : transitivité des majorants *)

Theorem bt_lt_trans : forall (t : BinaryTree) (n n' : nat),
  t << n -> n < n' -> t << n'.
Proof.
  induction t; intros.
    (* arbre vide *)
    apply bt_lt.
    intros.
    inversion H1.

    (* cas inductif *)
    apply bt_lt.
    intros x.
    intros x_in_tree.
    inversion_clear H.
    apply gt_trans with n0.
      exact H0.

      exact (H1 x x_in_tree).
Qed.

(** ** Minorant d’un arbre binaire

  Une valeur n minore un arbre binaire si elle est strictement inférieure
  à chacun des éléments de l’arbre. *)

Reserved Notation "T >> n" (at level 70, no associativity).

Inductive BinaryTreeGt : BinaryTree -> nat -> Prop :=
| bt_gt : forall (t : BinaryTree) (n : nat),
  (forall x : nat, x ∊ t -> x > n)
  -> t >> n
where "T >> n" := (BinaryTreeGt T n).

(** Tactique pour montrer qu’une valeur minore un arbre binaire *)

Ltac binary_tree_gt :=
  repeat
    match goal with
    | [ H : ?x ∊ ?t |- _ ] => inversion_clear H
    | [ |- ?T >> ?n] => apply bt_gt; intros
    | [ |- ?n0 > ?n] => omega
    end.

Goal [] >> 0.
Proof.
  binary_tree_gt.
Qed.

Goal ex_bt_single >> 0.
Proof.
  binary_tree_gt.
Qed.

Goal ~(ex_bt_small >> 2).
Proof.
  intro.
  inversion_clear H.
  pose (H0 2).
  cut (2 > 2).
  omega.
  apply g.
  apply in_tree_node.
Qed.

(** Théorème : transitivité des minorants *)

Theorem bt_gt_trans : forall (t : BinaryTree) (n n' : nat),
  t >> n -> n > n' -> t >> n'.
Proof.
  induction t; intros.
    (* arbre vide *)
    apply bt_gt.
    intros.
    inversion H1.

    (* cas inductif *)
    apply bt_gt.
    intros x.
    intros x_in_tree.
    inversion_clear H.
    apply gt_trans with n0.
      exact (H1 x x_in_tree).

      exact H0.
Qed.

(** ** Égalité du contenu de deux arbres

  Deux arbres binaires ont le même contenu lorsque tous les éléments du
  premier arbre sont dans le second et inversement. *)

Reserved Notation "T ≡ T'" (at level 70, no associativity).

Inductive BinaryTreeEq : BinaryTree -> BinaryTree -> Prop :=
| bt_eq : forall (t t' : BinaryTree),
  (forall k : nat, k ∊ t -> k ∊ t')
  -> (forall k : nat, k ∊ t' -> k ∊ t)
  -> t ≡ t'
where "T ≡ T'" := (BinaryTreeEq T T').

(** Exemple *)

Goal ex_bt_small ≡ ex_bt_other.
Proof.
  unfold ex_bt_small.
  unfold ex_bt_other.
  apply bt_eq.
    (* tous les éléments du premier arbre sont dans le second *)
    intros k.
    intros k_in_tree.
    inversion k_in_tree; subst.
      (* k à la racine *)
      apply in_tree_right.
      apply in_tree_node.

      (* k à la racine de la gauche *)
      inversion H1.
      apply in_tree_node.

      (* k à gauche de la gauche *)
      inversion H2.

      (* k à droite de la gauche *)
      inversion H2.

      (* k à droite *)
      inversion H1.

    (* tous les éléments du second arbre sont dans le premier *)
    intros k.
    intros k_in_tree.
    inversion k_in_tree; subst.
      (* k à la racine *)
      apply in_tree_left.
      apply in_tree_node.

      (* k à gauche *)
      inversion H1.

      (* k à droite *)
      inversion H1.
      apply in_tree_node.

      (* k à gauche de la droite *)
      inversion H2.

      (* k à droite de la droite *)
      inversion H2.
Qed.

(** Théorème : réflexivité de l’égalité du contenu *)

Theorem bt_eq_refl : forall (t : BinaryTree), t ≡ t.
Proof.
  induction t; apply bt_eq; auto.
Qed.

(** ** Arbre binaire de recherche

  Un arbre binaire de recherche (ABR) est un arbre binaire tel que
  les valeurs à gauche de la racine soient inférieures à la racine,
  les valeurs à droite de la racine soient supérieures à la racine
  et ce récursivement dans tous les sous-arbres. *)

Inductive BinarySearchTree : BinaryTree -> Prop :=
| bst_empty : BinarySearchTree []
| bst_node : forall (x : nat) (tleft tright : BinaryTree),
     tleft << x
  -> tright >> x
  -> BinarySearchTree tleft
  -> BinarySearchTree tright
  -> BinarySearchTree [x, tleft, tright].

(** Quelques exemples d’arbres binaires de recherche :

  - un arbre binaire de recherche contenant 10, 20, 30, 40 et 50 : *)
Example ex_bt_bst_1 :=
  [40,
    [20,
      [10],
      [30]
    ],
    [50]
  ].

(** - un arbre binaire de recherche déséquilibré avec les mêmes valeurs : *)
Example ex_bt_bst_2 :=
  [10,
    [],
    [20,
      [],
      [30,
        [],
        [40,
          [],
          [50]
        ]
      ]
    ]
  ].

(** - un arbre binaire de recherche moins déséquilibré avec les mêmes valeurs : *)
Example ex_bt_bst_3 :=
  [30,
    [10,
      [],
      [20]
    ],
    [40,
      [],
      [50]
    ]
  ].

(** - un arbre binaire en forme de serpent : *)
Example ex_bt_bst_4 :=
  [10,
    [],
    [50,
      [],
      [100,
        [75,
          [67,
            [],
            [70,
              [],
              [73,
                [72,
                  [71],
                  []
                ],
                []
              ]
            ]
          ],
          []
        ],
        []
      ]
    ]
  ].

(** Tactique pour montrer qu’un arbre binaire est un ABR *)

Ltac binary_search_tree :=
  repeat
    match goal with
    | |- context[BinarySearchTree []] => apply bst_empty
    | |- context[BinarySearchTree _] => apply bst_node
    | |- context[_ << _] => binary_tree_lt
    | |- context[_ >> _] => binary_tree_gt
    end.

Goal BinarySearchTree [].
  binary_search_tree.
Qed.

Goal BinarySearchTree ex_bt_bst_1.
  binary_search_tree.
Qed.

Goal BinarySearchTree ex_bt_bst_2.
  binary_search_tree.
Qed.

Goal BinarySearchTree ex_bt_bst_3.
  binary_search_tree.
Qed.

(** Théorème : si une valeur d’un arbre binaire de recherche est
    inférieure à la racine, elle se trouve à sa gauche *)

Theorem bst_in_tree_left : forall (x n : nat) (tleft tright : BinaryTree),
     BinarySearchTree [x, tleft, tright]
  -> n < x
  -> n ∊ [x, tleft, tright]
  -> n ∊ tleft.
Proof.
  intros x n tleft tright.
  intros BST n_lt_x n_in_t.
  inversion BST; subst.
  inversion n_in_t; subst.
    omega.

    exact H1.

    exfalso.
      inversion_clear H3.
      pose (H n H1).
      omega.
Qed.

(** Théorème : si une valeur d’un arbre binaire de recherche est
    supérieure à la racine, elle se trouve à sa droite *)

Theorem bst_in_tree_right : forall (x n : nat) (tleft tright : BinaryTree),
     BinarySearchTree [x, tleft, tright]
  -> n > x
  -> n ∊ [x, tleft, tright]
  -> n ∊ tright.
Proof.
  intros x n tleft tright.
  intros BST n_lt_x n_in_t.
  inversion BST; subst.
  inversion n_in_t; subst.
    omega.

    exfalso.
      inversion_clear H2.
      pose (H n H1).
      omega.

    exact H1.
Qed.

(** * Manipulation du type *)

(** ** Recherche dans un arbre binaire de recherche *)

(** Spécification de la recherche dans un arbre binaire quelconque

  Si la valeur recherchée appartient à l’arbre alors le résultat
  est « vrai ». Sinon, le résultat est « faux ». *)

Inductive BtSearchSpec : nat -> BinaryTree -> bool -> Prop :=
| bt_search_true : forall (n : nat) (t : BinaryTree),
    n ∊ t -> BtSearchSpec n t true
| bt_search_false : forall (n : nat) (t : BinaryTree),
    ~(n ∊ t) -> BtSearchSpec n t false.

(** Théorème : si une fonction de recherche trouve un élément dans l’un des
    sous-arbres d’un arbre binaire ou à sa racine, alors elle le trouve dans
    l’arbre en entier *)

Theorem bt_search_hereditary : forall (x n : nat) (tl tr : BinaryTree),
    BtSearchSpec n tl true \/ x = n \/ BtSearchSpec n tr true
    -> BtSearchSpec n [x, tl, tr] true.
Proof.
  intros x n tl tr.
  intros n_is_found.
  inversion_clear n_is_found.
    (* n trouvé à gauche *)
    inversion_clear H.
    apply bt_search_true.
    apply in_tree_hereditary.
    left.
    exact H0.

    inversion_clear H.
      (* n à la racine *)
      subst.
      apply bt_search_true.
      apply in_tree_node.

      (* n trouvé à droite *)
      inversion_clear H0.
      apply bt_search_true.
      apply in_tree_hereditary.
      right.
      exact H.
Qed.

Lemma bt_search_neg_true : forall (n : nat) (t : BinaryTree),
  ~(BtSearchSpec n t true)
  -> BtSearchSpec n t false.
Proof.
  intros.
  apply bt_search_false.
  intro.
  apply H.
  apply bt_search_true.
  exact H0.
Qed.

Lemma bt_search_neg_true_inv : forall (n : nat) (t : BinaryTree),
  BtSearchSpec n t false
  -> ~(BtSearchSpec n t true).
Proof.
  intros.
  intro.
  inversion_clear H.
  inversion_clear H0.
  exact (H1 H).
Qed.

Lemma bt_search_hereditary_neg : forall (x n : nat) (tleft tright : BinaryTree),
  ~(BtSearchSpec n tleft true \/ x = n \/ BtSearchSpec n tright true)
  -> BtSearchSpec n tleft false /\ x <> n /\ BtSearchSpec n tright false.
Proof.
  intros.
  split.
    apply bt_search_neg_true.
      intro.
      apply H.
      left.
      exact H0.

    split.
      intro.
      apply H.
      right.
      left.
      exact H0.

      apply bt_search_neg_true.
        intro.
        apply H.
        right.
        right.
        exact H0.
Qed.

(** Théorème : si une fonction de recherche ne trouve pas un élément dans un
    arbre, alors elle ne le trouve ni à sa racine, ni à sa gauche, ni à
    sa droite (par contraposée du théorème précédent) *)

Theorem bt_search_hereditary_ctr : forall (x n : nat) (tleft tright : BinaryTree),
  BtSearchSpec n [x, tleft, tright] false
  -> BtSearchSpec n tleft false /\ x <> n /\ BtSearchSpec n tright false.
Proof.
  intros x n tleft tright.
  cut (
    ~(BtSearchSpec n [x, tleft, tright] true)
    -> ~(BtSearchSpec n tleft true \/ x = n \/ BtSearchSpec n tright true)
  ).
    intros.
    apply bt_search_hereditary_neg.
    apply bt_search_neg_true_inv in H0.
    exact (H H0).

    intros.
    intro.
    apply bt_search_hereditary in H0.
    exact (H H0).
Qed.

(** Théorème : si une fonction de recherche ne trouve pas un élément dans les
    sous-arbres d’un arbre binaire ni à sa racine, alors elle ne le trouve
    pas dans l’arbre en entier *)

Theorem bt_search_hereditary_inv : forall (x n : nat) (tleft tright : BinaryTree),
  BtSearchSpec n tleft false /\ x <> n /\ BtSearchSpec n tright false
  -> BtSearchSpec n [x, tleft, tright] false.
Proof.
  intros x n tleft tright.
  intros n_not_found.
  inversion_clear n_not_found.
  inversion_clear H0.
  apply bt_search_false.
  inversion_clear H.
  inversion_clear H2.
  intro.
  inversion H2.
    omega.

    exact (H0 H5).

    exact (H H5).
Qed.

(** Recherche dichotomique dans un arbre binaire de recherche *)

Fixpoint bst_search (n : nat) (t : BinaryTree) : bool :=
  match t with
  (* rien ne peut être trouvé dans un arbre vide *)
  | [] => false
  | [x, tleft, tright] =>
    match (lt_eq_lt_dec n x) with
    (* si n < x, on va chercher n à gauche *)
    | inleft (left _)  => bst_search n tleft
    (* si n = x, on a trouvé n *)
    | inleft (right _) => true
    (* si n > x, on va chercher n à droite *)
    | inright _        => bst_search n tright
    end
  end.

Functional Scheme bst_search_ind :=
  Induction for bst_search Sort Prop.

(** Tests de l’implémentation *)

Print ex_bt_bst_1.
Compute bst_search 20 ex_bt_bst_1. (* = true *)
Compute bst_search 60 ex_bt_bst_1. (* = false *)
Compute bst_search 25 ex_bt_bst_1. (* = false *)

Print ex_bt_bst_2.
Compute bst_search 10 ex_bt_bst_2. (* = true *)
Compute bst_search 40 ex_bt_bst_2. (* = true *)
Compute bst_search 0 ex_bt_bst_2. (* = false *)

Print ex_bt_bst_3.
Compute bst_search 0 ex_bt_bst_3. (* = false *)
Compute bst_search 20 ex_bt_bst_3. (* = true *)
Compute bst_search 60 ex_bt_bst_3. (* = false *)

(** Théorème de correction de la recherche dichotomique *)

Theorem bst_search_sound : forall (n : nat) (t : BinaryTree) (res : bool),
  BinarySearchTree t -> bst_search n t = res -> BtSearchSpec n t res.
Proof.
  intros n t.
  functional induction (bst_search n t).
    (* arbre vide *)
    intros.
    subst.
    apply bt_search_false.
    intro.
    inversion H0.

    (* arbre non-vide et n < x *)
    clear e0.
    induction res.

      (* l’algo répond vrai *)
      intros BST found_left.
      apply bt_search_hereditary.
      left.
      apply IHb.
        inversion_clear BST.
        exact H1.

        exact found_left.

      (* l’algo répond faux *)
      intros BST not_found_left.
      inversion_clear BST.
      apply bt_search_hereditary_inv.
      split.
        apply IHb.
          exact H1.

          exact not_found_left.

        split.
          omega.

          apply bt_search_false.
          intro.
          inversion_clear H0.
          pose (H4 n H3).
          omega.

    (* arbre non-vide et n = x *)
    clear e0.
    intros res.
    intros BST res_is_true.
    subst.
    apply bt_search_true.
    apply in_tree_node.

    (* arbre non-vide et n > x *)
    clear e0.
    induction res.

      (* l’algo répond vrai *)
      intros BST found_right.
      apply bt_search_hereditary.
      right.
      right.
      apply IHb.
        inversion_clear BST.
        exact H2.

        exact found_right.

      (* l’algo répond faux *)
      intros BST not_found_right.
      inversion_clear BST.
      apply bt_search_hereditary_inv.
      split.
        apply bt_search_false.
        intro.
        inversion_clear H.
        pose (H4 n H3).
        omega.

        split.
          omega.

          apply IHb.
            exact H2.

            exact not_found_right.
Qed.

(** Théorème de complétude de la recherche dichotomique *)

Theorem bst_search_complete : forall (n : nat) (t : BinaryTree) (res : bool),
  BinarySearchTree t -> BtSearchSpec n t res -> bst_search n t = res.
Proof.
  intros n t.
  functional induction (bst_search n t).
    (* arbre vide *)
    intros.
    inversion_clear H0.
      inversion H1.

      reflexivity.

    (* arbre non-vide et n < x *)
    clear e0.
    induction res.
      (* l’élément est dans l’arbre *)
      intros BST n_in_t.
      inversion_clear n_in_t.
      apply IHb.
        inversion_clear BST.
        exact H2.

        apply bt_search_true.
        apply bst_in_tree_left with x tright; auto.

      (* l’élément n’est pas dans l’arbre *)
      intros BST n_not_in_t.
      apply IHb.
        inversion BST.
        auto.

        apply bt_search_hereditary_ctr in n_not_in_t.
        inversion_clear n_not_in_t.
        exact H.

    (* arbre non-vide et n = x *)
    clear e0.
    induction res.
      intros BST n_in_t.
      reflexivity.

      intros BST n_not_in_t.
      exfalso.
        inversion_clear n_not_in_t.
        apply H.
        apply in_tree_node.

    (* arbre non-vide et n > x *)
    clear e0.
    induction res.
      (* l’élément est dans l’arbre *)
      intros BST n_in_t.
      inversion_clear n_in_t.
      apply IHb.
        inversion_clear BST.
        exact H3.

        apply bt_search_true.
        apply bst_in_tree_right with x tleft; auto.

      (* l’élément n’est pas dans l’arbre *)
      intros BST n_not_in_t.
      apply IHb.
        inversion BST.
        auto.

        apply bt_search_hereditary_ctr in n_not_in_t.
        inversion_clear n_not_in_t.
        inversion_clear H0.
        exact H2.
Qed.

(** ** Insertion dans un arbre binaire de recherche *)

(** Spécification de l’insertion sans doublon dans un arbre binaire quelconque *)

Inductive BtInsertSpec : nat -> BinaryTree -> BinaryTree -> Prop :=
| bt_insert : forall (n : nat) (t t' : BinaryTree),
    (* tous les éléments de l’arbre initial doivent rester *)
    (forall (k : nat), k ∊ t -> k ∊ t')
    (* tous les éléments du résultat, hormis n, doivent venir de l’entrée *)
    -> (forall (k : nat), k ∊ t' -> k <> n -> k ∊ t)
    (* n doit être inséré dans le résultat *)
    -> n ∊ t'
    -> BtInsertSpec n t t'.

(** Insertion dans un arbre binaire de recherche *)

Fixpoint bst_insert (n : nat) (t : BinaryTree) : BinaryTree :=
  match t with
  (* insertion dans l’arbre vide : création d’une feuille *)
  | bt_empty => [n]
  | bt_node x tleft tright =>
    match (lt_eq_lt_dec n x) with
      (* si n < x, on va insérer n à gauche *)
      | inleft (left _)  => bt_node x (bst_insert n tleft) tright
      (* si n = x, n était déjà dans l’arbre et on ne change rien *)
      | inleft (right _) => bt_node x tleft tright
      (* si n > x, on va insérer n à droite *)
      | inright _        => bt_node x tleft (bst_insert n tright)
    end
  end.

Functional Scheme bst_insert_ind :=
  Induction for bst_insert Sort Prop.

(** Tests de l’implémentation *)

Print ex_bt_bst_1.
Compute bst_insert 20 ex_bt_bst_1.
Compute bst_insert 60 ex_bt_bst_1.
Compute bst_insert 5 ex_bt_bst_1.

Print ex_bt_bst_2.
Compute bst_insert 5 ex_bt_bst_2.
Compute bst_insert 35 ex_bt_bst_2.
Compute bst_insert 100 ex_bt_bst_2.

Print ex_bt_bst_3.
Compute bst_insert 20 ex_bt_bst_3.
Compute bst_insert 60 ex_bt_bst_3.
Compute bst_insert 15 ex_bt_bst_3.

Print ex_bt_bst_4.
Compute bst_insert 110 ex_bt_bst_4.
Compute bst_insert 76 ex_bt_bst_4.
Compute bst_insert 15 ex_bt_bst_4.

(** Théorème de correction de l'insertion *)

Theorem bst_insert_sound : forall (n : nat) (t t' : BinaryTree),
  BinarySearchTree t
  -> bst_insert n t = t'
  -> BtInsertSpec n t t' /\ BinarySearchTree t'.
Proof.
  intros n t.
  functional induction (bst_insert n t).
    (* arbre vide *)
    intros.
    rewrite <- H0.
    split.
      apply bt_insert.
        intros.
        inversion H1.

        intros.
        exfalso.
        inversion H1.
          omega.

          inversion H5.

          inversion H5.
        apply in_tree_node.
      binary_search_tree.

    (* cas insertion gauche *)
    intros.
    subst.
    inversion H.
    pose (IHb ( bst_insert n tleft ) H5 (eq_refl)).
    inversion_clear a.
    split.
      apply bt_insert.
        intros.
        inversion H9.
          apply in_tree_node.

          apply in_tree_left.
          subst.
          inversion H7.
          apply H0.
          exact H12.

          subst.
          apply in_tree_right.
          exact H12.

        intros.
        subst.
        inversion H9; subst.
          apply in_tree_node.

          inversion H7.
          apply in_tree_left.
          apply H1; auto.

          apply in_tree_right.
          exact H2.

        subst.
        inversion H7.
        subst.
        apply in_tree_left.
        exact H2.

        subst.
        apply bst_node; try assumption.
          apply bt_lt.
          intros.
          inversion H7.
          subst.
          elim (Nat.eq_dec x0 n).
            intro.
            subst.
            exact _x.

            intro.
            pose (H2 x0 H0 b).
            inversion_clear H3.
            exact (H10 x0 i).

    (* cas où on fait rien *)
    intros.
    split.
      rewrite H0.
      subst.
      apply bt_insert; auto.
      apply in_tree_node.

      subst.
      exact H.

    (* cas insertion droite *)
    intros.
    subst.
    inversion H.
    pose (IHb ( bst_insert n tright ) H6 (eq_refl)).
    inversion_clear a.
    split.
      apply bt_insert.
      intros.
      inversion H9.
        apply in_tree_node.

        apply in_tree_left.
        exact H12.

        subst.
        apply in_tree_right.
        inversion H7.
        apply H0.
        exact H12.

      intros.
      subst.
      inversion H9; subst.
        apply in_tree_node.

        inversion H7.
        apply in_tree_left.
        exact H2.

        inversion H7.
        apply in_tree_right.
        apply H1; auto.

      subst.
      inversion H7.
      subst.
      apply in_tree_right.
      exact H2.

    subst.
    apply bst_node; try assumption.
    apply bt_gt.
    intros.
    inversion H7.
    subst.
    elim (Nat.eq_dec x0 n).
      intro.
      subst.
      exact _x.

      intro.
      pose (H2 x0 H0 b).
      inversion_clear H4.
      exact (H10 x0 i).
Qed.

(** Théorème de complétude de l'insertion *)

Theorem bst_insert_complete : forall (n : nat) (t t' t'' : BinaryTree),
  BinarySearchTree t
  -> BtInsertSpec n t t'
  -> bst_insert n t = t''
  -> t' ≡ t''.
Proof.
  intros n t.
  functional induction (bst_insert n t); intros; subst; try clear e0.
    (* arbre vide *)
    inversion H0.
    subst.
    apply bt_eq.
      intro k.
      intro k_in_t'.
      elim (Nat.eq_dec k n).
        intro; subst.
        apply in_tree_node.

        intro k_neq_n.
        pose (H2 k k_in_t' k_neq_n).
        inversion i.

      intro k.
      intro k_is_n.
      inversion k_is_n; subst.
        exact H3.

        inversion H6.

        inversion H6.

    (* cas insertion gauche *)
    inversion H0; subst.
    inversion H; subst.
    pose (bst_insert_sound n tleft (bst_insert n tleft) H9 eq_refl).
    inversion a.
    clear H7 H8 H9 H10 a.
    apply bt_eq.
      intros k.
      intros k_in_t'.
      elim (Nat.eq_dec n k).
        intro; subst.
        apply in_tree_left.
        simpl.
        inversion_clear H4.
        exact H8.

        intro n_neq_k.
        pose (H2 k k_in_t' (not_eq_sym n_neq_k)).
        inversion i; subst.
          apply in_tree_node.

          apply in_tree_left.
          inversion_clear H4.
          apply H6.
          exact H8.

          apply in_tree_right.
          exact H8.

      intros k.
      intros k_in_ans.
      inversion k_in_ans; subst.
        apply H1.
        apply in_tree_node.

        elim (Nat.eq_dec k n).
          intros; subst.
          exact H3.

          intros k_neq_n.
          inversion_clear H4.
          pose (H7 k H8 k_neq_n).
          apply H1.
          apply in_tree_left.
          exact i.

        apply H1.
        apply in_tree_right.
        exact H8.

    (* cas où on fait rien *)
    apply bt_eq.
      intros k.
      intros k_in_t'.
      inversion_clear H0.
      elim (Nat.eq_dec k n).
        intros; subst.
        apply in_tree_node.

        intros k_neq_n.
        apply H2.
          exact k_in_t'.

          exact k_neq_n.

      inversion_clear H0.
      exact H1.

    (* cas insertion droite *)
    inversion H0; subst.
    inversion H; subst.
    pose (bst_insert_sound n tright (bst_insert n tright) H10 eq_refl).
    inversion a.
    clear H7 H8 H9 H10 a.
    apply bt_eq.
      intros k.
      intros k_in_t'.
      elim (Nat.eq_dec n k).
        intro; subst.
        apply in_tree_right.
        simpl.
        inversion_clear H4.
        exact H8.

        intro n_neq_k.
        pose (H2 k k_in_t' (not_eq_sym n_neq_k)).
        inversion i; subst.
          apply in_tree_node.

          apply in_tree_left.
          exact H8.

          apply in_tree_right.
          inversion_clear H4.
          apply H6.
          exact H8.

      intros k.
      intros k_in_ans.
      inversion k_in_ans; subst.
        apply H1.
        apply in_tree_node.

        apply H1.
        apply in_tree_left.
        exact H8.

        elim (Nat.eq_dec k n).
          intros; subst.
          exact H3.

          intros k_neq_n.
          inversion_clear H4.
          pose (H7 k H8 k_neq_n).
          apply H1.
          apply in_tree_right.
          exact i.
Qed.

(** ** Recherche du maximum d’un arbre binaire de recherche *)

(** Spécification du maximum d’un arbre binaire

  Le maximum d’un arbre binaire est un élément de l’arbre qui
  est supérieur ou égal à chacun des éléments de l’arbre. *)

Inductive BinaryTreeMax : BinaryTree -> option nat -> Prop :=
| bt_max_empty : BinaryTreeMax [] None
| bt_max_node : forall (max : nat) (t : BinaryTree),
    max ∊ t
    -> (forall n : nat, n ∊ t -> n <= max)
    -> BinaryTreeMax t (Some max).

(** Lemme : le maximum du sous-arbre droit d’un arbre binaire de recherche
    est le maximum de son arbre binaire de recherche parent *)

Lemma bst_max_right : forall (max : option nat) (x xright : nat) (tleft tright trightleft trightright : BinaryTree),
  tright = [xright, trightleft, trightright]
  -> BinarySearchTree [x, tleft, tright]
  -> BinaryTreeMax tright max
  -> BinaryTreeMax [x, tleft, tright] max.
Proof.
  intros max x xright tleft tright trightleft trightright.
  intros right_not_empty BST right_max.
  inversion right_max; subst.
    (* pas de maximum à droite : impossible car l’arbre n’est pas vide *)
    inversion H.

    apply bt_max_node.
      (* le maximum est dans l’arbre *)
      apply in_tree_right.
      exact H.

      (* preuve que tout n de l’arbre est plus petit que le max de la droite *)
      intro n.
      intro n_in_tree.
      inversion n_in_tree; subst.
        (* n est à la racine *)
        apply le_trans with xright.
          inversion_clear BST.
          inversion_clear H2.
          apply Nat.lt_le_incl.
          apply H5.
          apply in_tree_node.

          apply H0.
          apply in_tree_node.

        (* n est à gauche *)
        apply le_trans with x.
          inversion_clear BST.
          inversion_clear H1.
          apply Nat.lt_le_incl.
          apply H6.
          exact H3.

          apply le_trans with xright.
            inversion_clear BST.
            inversion_clear H2.
            apply Nat.lt_le_incl.
            apply H6.
            apply in_tree_node.

            apply H0.
            apply in_tree_node.

        (* n est à droite *)
        apply H0.
        exact H3.
Qed.

(** Lemme : le maximum d’un arbre binaire de recherche dont le sous-arbre droit
    n’est pas vide est le maximum de ce sous-arbre droit *)

Lemma bst_max_right_inv : forall (max : option nat) (x xright : nat) (tleft tright trightleft trightright : BinaryTree),
  tright = [xright, trightleft, trightright]
  -> BinarySearchTree [x, tleft, tright]
  -> BinaryTreeMax [x, tleft, tright] max
  -> BinaryTreeMax tright max.
Proof.
  intros max x xright tleft tright trightleft trightright.
  intros right_not_empty BST ans_max.
  inversion ans_max; subst.
    apply bt_max_node.
      (* le maximum est dans l’arbre *)
      inversion H; subst.
        (* le maximum est la racine : contradiction *)
        exfalso.
          inversion_clear BST.
          inversion_clear H2.
          pose (H5 xright (in_tree_node xright trightleft trightright)).
          apply gt_not_le in g.
          apply g.
          apply H0.
          apply in_tree_right.
          apply in_tree_node.

        (* le maximum est à gauche : contradiction *)
        exfalso.
          inversion_clear BST.
          inversion_clear H1.
          pose (H6 max0 H3).
          apply gt_not_le in l.
          apply l.
          apply H0.
          apply in_tree_node.

        (* le maximum est à droite : on a gagné *)
        exact H3.

      (* le maximum est un maximum *)
      intros n.
      intros n_in_tright.
      apply H0.
      apply in_tree_right.
      exact n_in_tright.
Qed.

(** Lemme : le maximum d’un arbre non-vide existe *)

Lemma bt_max_exists : forall (x : nat) (max : option nat) (tleft tright : BinaryTree),
  BinaryTreeMax [x, tleft, tright] max
  -> max <> None.
Proof.
  intros x max tleft tright.
  intros max_is_max.
  inversion max_is_max.
  discriminate.
Qed.

(** Recherche dichotomique du maximum d’un arbre binaire de recherche *)

Fixpoint bst_max (t : BinaryTree) : option nat :=
  match t with
  (* le maximum d’un arbre vide n’existe pas *)
  | [] => None
  | [x, tleft, tright] =>
    match tright with
    (* un arbre sans sous-arbre droit a pour maximum sa racine *)
    | [] => Some x
    (* sinon on va chercher le maximum à droite *)
    | _  => bst_max tright
    end
  end.

Functional Scheme bst_max_ind :=
  Induction for bst_max Sort Prop.

(** Tests de la recherche du maximum *)

Print ex_bt_bst_1.
Compute bst_max ex_bt_bst_1. (* = Some 50 *)

Print ex_bt_bst_2.
Compute bst_max ex_bt_bst_2. (* = Some 50 *)

Print ex_bt_bst_3.
Compute bst_max ex_bt_bst_3. (* = Some 50 *)

Print ex_bt_bst_4.
Compute bst_max ex_bt_bst_4. (* = Some 100 *)

Compute bst_max []. (* None *)

(** Théorème de correction de la fonction de recherche dichotomique
    du maximum d’un arbre binaire de recherche *)

Theorem bst_max_sound : forall (max : option nat) (t : BinaryTree),
  BinarySearchTree t -> bst_max t = max -> BinaryTreeMax t max.
Proof.
  intros max t.
  functional induction (bst_max t);
    intros BST ans;
    subst.
    (* maximum d’un arbre vide *)
    apply bt_max_empty.

    (* maximum d’un arbre sans sous-arbre droit *)
    apply bt_max_node.
      (* le maximum est la racine *)
      apply in_tree_node.

      (* la racine dépasse tout le monde *)
      intros n.
      intros n_in_tree.
      inversion n_in_tree; subst.
        (* elle se dépasse elle-même *)
        reflexivity.

        (* elle dépasse le sous-arbre gauche par la propriété d’ABR *)
        inversion_clear BST.
        inversion_clear H.
        apply Nat.lt_le_incl.
        apply H4.
        exact H1.

        (* et le sous-arbre droit est vide *)
        inversion H1.

    (* maximum d’un arbre ayant un sous-arbre droit *)
    apply bst_max_right with _x0 _x1 _x2.
      reflexivity.

      exact BST.

      apply IHo.
        inversion_clear BST.
        exact H2.

        reflexivity.
Qed.

(** Théorème de complétude de la fonction de recherche dichotomique
    du maximum d’un arbre binaire de recherche *)

Theorem bst_max_complete : forall (max : option nat) (t : BinaryTree),
  BinarySearchTree t -> BinaryTreeMax t max -> bst_max t = max.
Proof.
  intros max t.
  functional induction (bst_max t);
    intros BST max_is_max;
    subst.
    (* maximum d’un arbre vide *)
    inversion max_is_max.
      reflexivity.

      inversion H.

    (* maximum d’un arbre sans sous-arbre droit *)
    inversion max_is_max; subst.
      inversion H.
        (* si le maximum est la racine, notre algo a raison *)
        reflexivity.

        (* si le maximum est à gauche, contradiction *)
        exfalso.
          inversion_clear BST.
          inversion_clear H6.
          pose (H0 x (in_tree_node x _x [])).
          apply le_not_gt in l.
          apply l.
          apply H10.
          exact H3.

        (* si le maximum est à droite, contradiction *)
        inversion H3.

    (* maximum d’un arbre ayant un sous-arbre droit *)
    inversion max_is_max; subst.
      apply IHo.
        inversion_clear BST.
        exact H4.
        apply bst_max_right_inv with x _x0 _x _x1 _x2.
          reflexivity.

          exact BST.

          exact max_is_max.
Qed.

(** ** Suppression d’un élément dans un arbre binaire de recherche *)

(** Spécification des fonctions de suppression dans un arbre binaire sans doublon *)

Inductive BtRemoveSpec : nat -> BinaryTree -> BinaryTree -> Prop :=
| bt_remove : forall (n : nat) (t t' : BinaryTree),
    (forall (k : nat), k ∊ t -> k <> n -> k ∊ t')
    -> (forall (k : nat), k ∊ t' -> k ∊ t)
    -> ~(n ∊ t')
    -> BtRemoveSpec n t t'.

(** Suppression dichotomique dans un arbre binaire de recherche *)

Fixpoint bst_remove (n : nat) (t : BinaryTree) : BinaryTree :=
  match t with
  | [] => []
  | [x, tleft, tright] =>
    match (lt_eq_lt_dec n x) with
    (* la valeur à supprimer se trouve dans le sous-arbre gauche *)
    | inleft (left _)   => [x, (bst_remove n tleft), tright]
    (* la valeur à supprimer se trouve dans le sous-arbre droit *)
    | inright _         => [x, tleft, (bst_remove n tright)]
    (* on a trouvé le nœud contenant la valeur à supprimer *)
    | inleft (right _)  =>
      match tleft, tright with
      (* le nœud à supprimer est une feuille :
         on le remplace par l’arbre vide *)
      | [], [] => []
      (* le nœud à supprimer n’a qu’un enfant :
         on le remplace par cet enfant *)
      | [_, _, _], [] => tleft
      | [], [_, _, _] => tright
      (* le nœud à supprimer a deux enfants :
         on va l’échanger avec son prédécesseur *)
      | [_, _, _], [_, _, _] =>
        let prev := (bst_max tleft) in
        match prev with
        (* cas impossible, puisque le sous-arbre gauche n’est pas vide
           on a nécessairement un prédécesseur *)
        | None => []
        (* remplacement par un nœud qui a pour étiquette le prédécesseur
           et pour fils les sous-arbres initiaux en supprimant le prédécesseur
           du sous-arbre gauche *)
        | Some xpred =>
          [xpred, (bst_remove xpred tleft), tright]
        end
      end
    end
  end.

Functional Scheme bst_remove_ind :=
  Induction for bst_remove Sort Prop.

(** Tests de la suppression dichotomique *)

Print ex_bt_bst_1.
Compute bst_remove 50 ex_bt_bst_1. (* = [40, [20, [10], [30]], []] *)
Compute bst_remove 20 ex_bt_bst_1. (* = [40, [10, [], [30]], [50]] *)
Compute bst_remove 40 ex_bt_bst_1. (* = [30, [20, [10], []], [50]] *)

Print ex_bt_bst_2.
Compute bst_remove 50 ex_bt_bst_2. (* = [10, [], [20, [], [30, [], [40]]]] *)
Compute bst_remove 30 ex_bt_bst_2. (* = [10, [], [20, [], [40, [], [50]]]] *)

Print ex_bt_bst_3.
Compute bst_remove 30 ex_bt_bst_3. (* = [20, [10], [40, [], [50]]] *)

Print ex_bt_bst_4.
Compute bst_remove 67 ex_bt_bst_4.

(** Théorème de correction de la suppression dichotomique *)

Theorem bst_remove_sound : forall (n : nat) (t t' : BinaryTree),
  BinarySearchTree t
  -> bst_remove n t = t'
  -> BtRemoveSpec n t t'
  /\ BinarySearchTree t'.
Proof.
  intros n t.
  functional induction (bst_remove n t);
    try clear e0;
    intros t';
    intros BST ans;
    subst.

    (* arbre vide *)
    clear BST.
    split; auto.
      apply bt_remove; auto.
        intro.
        inversion H.

        apply bst_empty.

    (* arbre non-vide et la valeur à supprimer se trouve à gauche *)
    inversion_clear BST.
    pose (IHb (bst_remove n tleft) H1 eq_refl).
    inversion_clear a.
    split.
      (* le résultat satisfait la spécification de suppression *)
      apply bt_remove; intro k; inversion H3.
        (* tout élément k de l’ancien arbre y reste, hormis n *)
        intros k_in_tree k_neq_n.
        inversion_clear k_in_tree.
          (* k = x *)
          apply in_tree_node.

          (* k < x *)
          pose (H5 k H11 k_neq_n).
          apply in_tree_hereditary.
          left.
          exact i.

          (* k > x *)
          apply in_tree_hereditary.
          right.
          exact H11.

        (* tout élément k du résultat était déjà dans l’ancien arbre *)
        intros k_in_res.
        inversion_clear k_in_res.
          (* k = x *)
          apply in_tree_node.

          (* k < x *)
          pose (H6 k H11).
          apply in_tree_hereditary.
          left.
          exact i.

          (* k > x *)
          apply in_tree_hereditary.
          right.
          exact H11.

        (* n n’est pas dans le résultat *)
        inversion k; subst.
          (* n est à la racine : exclu *)
          omega.

          (* n est à gauche : exclu par hypothèse d’induction *)
          apply H7.
          exact H13.

          (* n est à droite : exclu par propriété d’ABR *)
          inversion_clear H0.
          pose (H8 n H13).
          omega.

      (* le résultat est un arbre binaire de recherche *)
      apply bst_node; try assumption.
        apply bt_lt.
        intros x0.
        intros x0_in_tleft_rem.
        inversion H3.
        subst.
        apply H6 in x0_in_tleft_rem.
        inversion H.
        exact (H8 x0 x0_in_tleft_rem).

    (* arbre non-vide et la valeur à supprimer se trouve à droite *)
    6: {
    inversion_clear BST.
    pose (IHb (bst_remove n tright) H2 eq_refl).
    inversion_clear a.
    split.
      (* le résultat satisfait la spécification de suppression *)
      apply bt_remove; intro k; inversion H3.
        (* tout élément k de l’ancien arbre y reste, hormis n *)
        intros k_in_tree k_neq_n.
        inversion_clear k_in_tree.
          (* k = x *)
          apply in_tree_node.

          (* k < x *)
          apply in_tree_hereditary.
          left.
          exact H11.

          (* k > x *)
          pose (H5 k H11 k_neq_n).
          apply in_tree_hereditary.
          right.
          exact i.

        (* tout élément k du résultat était déjà dans l’ancien arbre *)
        intros k_in_res.
        inversion_clear k_in_res.
          (* k = x *)
          apply in_tree_node.

          (* k < x *)
          apply in_tree_hereditary.
          left.
          exact H11.

          (* k > x *)
          pose (H6 k H11).
          apply in_tree_hereditary.
          right.
          exact i.

        (* n n’est pas dans le résultat *)
        inversion k; subst.
          (* n est à la racine : exclu *)
          omega.

          (* n est à gauche : exclu par propriété d’ABR *)
          inversion_clear H.
          pose (H8 n H13).
          omega.

          (* n est à droite : exclu par hypothèse d’induction *)
          apply H7.
          exact H13.

      (* le résultat est un arbre binaire de recherche *)
      apply bst_node; try assumption.
        apply bt_gt.
        intros x0.
        intros x0_in_tright_rem.
        inversion H3.
        subst.
        apply H6 in x0_in_tright_rem.
        inversion H0.
        exact (H8 x0 x0_in_tright_rem).
    }

    (* supprimer la racine quand l’arbre est une feuille *)
    split.
      (* le résultat satisfait la spécification de suppression *)
      apply bt_remove.
        (* tout élément k de l’ancien arbre y reste, hormis n *)
        intros k.
        intros k_in_tree k_diff_n.
        inversion k_in_tree; subst; try omega; try inversion H1.

        (* tout élément k du résultat était déjà dans l’ancien arbre *)
        intros k.
        intros k_in_empty.
        inversion k_in_empty.

        (* n n’est pas dans le résultat *)
        intro n_in_empty.
        inversion n_in_empty.

      (* le résultat est un arbre binaire de recherche *)
      apply bst_empty.

    (* supprimer la racine quand l’arbre n’a qu’un fils droit *)
    split.
      (* le résultat satisfait la spécification de suppression *)
      apply bt_remove.
        (* tout élément k de l’ancien arbre y reste, hormis n *)
        intros k.
        intros k_in_tree k_diff_n.
        inversion k_in_tree; subst.
          omega.

          inversion H1.

          exact H1.

        (* tout élément k du résultat était déjà dans l’ancien arbre *)
        intros k.
        intros k_in_tree.
        apply in_tree_right.
        exact k_in_tree.

        (* n n’est pas dans le résultat *)
        intro n_in_right.
        inversion_clear BST.
        inversion_clear H0.
        pose (H3 n n_in_right).
        omega.

      (* le résultat est un arbre binaire de recherche *)
      inversion_clear BST.
      exact H2.

    (* supprimer la racine quand l’arbre n’a qu’un fils gauche *)
    split.
      (* le résultat satisfait la spécification de suppression *)
      apply bt_remove.
        (* tout élément k de l’ancien arbre y reste, hormis n *)
        intros k.
        intros k_in_tree k_diff_n.
        inversion k_in_tree; subst.
          omega.

          exact H1.

          inversion H1.

        (* tout élément k du résultat était déjà dans l’ancien arbre *)
        intros k.
        intros k_in_tree.
        apply in_tree_left.
        exact k_in_tree.

        (* n n’est pas dans le résultat *)
        intro n_in_left.
        inversion_clear BST.
        inversion_clear H.
        pose (H3 n n_in_left).
        omega.

      (* le résultat est un arbre binaire de recherche *)
      inversion_clear BST.
      exact H1.

    (* cas impossible *)
    2:{
    exfalso.
      inversion_clear BST.
      pose (bst_max_sound (bst_max [_x0, _x1, _x2]) [_x0, _x1, _x2] H1 eq_refl).
      pose (bt_max_exists _x0 (bst_max [_x0, _x1, _x2]) _x1 _x2 b).
      apply n0.
      exact e4.
    }

    (* supprimer la racine quand l’arbre a deux sous-arbres *)
    inversion_clear BST.
    pose (IHb (bst_remove xpred [_x0, _x1, _x2]) H1 eq_refl).
    inversion_clear a.
    apply bst_max_sound in e4.
      2: { exact H1. }
      split.
        (* le résultat satisfait la spécification de suppression *)
        apply bt_remove.
          (* tout élément k de l’ancien arbre y reste, hormis n *)
          intros k.
          intros k_in_tree k_neq_n.
          elim (Nat.eq_dec k xpred).
            (* k était le prédécesseur de n *)
            intros.
            subst.
            apply in_tree_node.

            (* k n’était pas le prédécesseur de n *)
            intros k_neq_xpred.
            inversion k_in_tree; subst.
              (* k = n impossible *)
              omega.

            (* k < n *)
            inversion H3; subst.
            apply in_tree_left.
            apply H5.
              exact H7.

                exact k_neq_xpred.

              (* k > n *)
              apply in_tree_right.
              exact H7.

          (* tout élément k du résultat était déjà dans l’ancien arbre *)
          intros k.
          intros k_in_res.
          inversion k_in_res; subst.
            (* k était le prédécesseur de n *)
            apply in_tree_left.
            inversion e4; subst.
            exact H6.

            (* k est inférieur au prédécesseur de n *)
            apply in_tree_left.
              inversion H3.
              apply H6 in H7.
              exact H7.

            (* k est supérieur au prédécesseur de n *)
            apply in_tree_right.
            exact H7.

          (* n n’est pas dans le résultat *)
          inversion e4; subst.
          intro n_in_result.
          inversion n_in_result; subst.
            (* n est son propre prédécesseur : exclu *)
            inversion_clear H.
            pose (H5 xpred H6).
            omega.

            (* n est dans le résultat de la suppression à gauche : exclu par HI *)
            inversion H3; subst.
            apply H7 in H9.
            inversion_clear H.
            pose (H11 n H9).
            omega.

            (* n est à droite : exclu par propriété d’ABR *)
            inversion_clear H0.
            pose (H5 n H9).
            omega.

        (* le résultat est un arbre binaire de recherche *)
        inversion e4; subst.
        inversion H3; subst.
        apply bst_node.
          (* l’arbre gauche reste inférieur à la racine *)
          apply bt_lt.
            intros x.
            intros x_in_tleft_rem_xpred.
            elim (Nat.eq_dec x xpred).
              (* x était le prédécesseur : exclu *)
              intros; subst.
              exfalso.
                apply H9.
                exact x_in_tleft_rem_xpred.

              (* x n’était pas le prédécesseur *)
              intros x_neq_xpred.
              apply H7 in x_in_tleft_rem_xpred.
              pose (H8 x x_in_tleft_rem_xpred).
              omega.

          (* l’arbre droit reste inférieur à la racine *)
          inversion_clear H.
          pose (H10 xpred H6).
          apply bt_gt_trans with n.
            exact H0.

            exact l.

          (* l’arbre gauche est un arbre binaire de recherche *)
          exact H4.

          (* l’arbre droit est un arbre binaire de recherche *)
          exact H2.
Qed.

(** Théorème de complétude de la suppression dichotomique *)

Theorem bst_remove_complete : forall (n : nat) (t t' t'' : BinaryTree),
  BinarySearchTree t
  -> BtRemoveSpec n t t'
  -> bst_remove n t = t''
  -> t' ≡ t''.
Proof.
  intros n t.
  functional induction (bst_remove n t); intros; subst.
    (* arbre vide *)
    apply bt_eq.
      intros.
      inversion_clear H0.
      apply H3.
      exact H1.

      intros.
      inversion H1.

    (* arbre non-vide et la valeur à supprimer se trouve à gauche *)
    inversion H; subst.
    pose (bst_remove_sound n tleft (bst_remove n tleft) H6 eq_refl).
    inversion a.
    clear H4 H5 H6 H7 a.
    apply bt_eq.
      intros.
      inversion_clear H0.
      pose (H5 k H3).
      inversion i; subst.
        apply in_tree_node.

        apply in_tree_left.
        elim (Nat.eq_dec n k).
          intros; subst.
          exfalso.
            apply H6.
            exact H3.

          intros.
          inversion_clear H1.
          apply H0.
            exact H8.

            apply not_eq_sym.
            exact b.

        apply in_tree_right.
        exact H8.

      intros.
      inversion H3; subst.
        inversion_clear H0.
        apply H4.
          apply in_tree_node.

          apply not_eq_sym.
          apply Nat.lt_neq.
          exact _x.

        elim (Nat.eq_dec n k).
          intros; subst.
          inversion_clear H1.
          exfalso.
            apply H7.
            exact H6.

          intros.
          inversion_clear H1.
            pose (H5 k H6).
            pose (in_tree_left x k tleft tright i).
            inversion_clear H0.
            apply H1.
              exact i0.

              apply not_eq_sym.
              exact b.

        elim (Nat.eq_dec n k).
          intros; subst.
          inversion_clear H.
          inversion_clear H5.
          pose (H k H6).
          omega.

          intros n_neq_k.
          inversion_clear H0.
            apply H4.
              apply in_tree_right.
              exact H6.

            apply not_eq_sym.
            exact n_neq_k.

    (* arbre non-vide et la valeur à supprimer se trouve à droite *)
    6: {
    inversion H; subst.
    pose (bst_remove_sound n tright (bst_remove n tright) H7 eq_refl).
    inversion a.
    clear H4 H5 H6 H7 a.
    apply bt_eq.
      intros.
      inversion_clear H0.
      pose (H5 k H3).
      inversion i; subst.
        apply in_tree_node.

        apply in_tree_left.
        exact H8.

        apply in_tree_right.
        elim (Nat.eq_dec n k).
          intros; subst.
          exfalso.
            apply H6.
            exact H3.

          intros.
          inversion_clear H1.
          apply H0.
            exact H8.

            apply not_eq_sym.
            exact b.

      intros.
      inversion H3; subst.
        inversion_clear H0.
        apply H4.
          apply in_tree_node.

          apply Nat.lt_neq.
          exact _x.

        elim (Nat.eq_dec n k).
          intros; subst.
          inversion_clear H.
          inversion_clear H4.
          pose (H k H6).
          omega.

          intros n_neq_k.
          inversion_clear H0.
            apply H4.
              apply in_tree_left.
              exact H6.

            apply not_eq_sym.
            exact n_neq_k.

        elim (Nat.eq_dec n k).
          intros; subst.
          inversion_clear H1.
          exfalso.
            apply H7.
            exact H6.

          intros.
          inversion_clear H1.
            pose (H5 k H6).
            pose (in_tree_right x k tleft tright i).
            inversion_clear H0.
            apply H1.
              exact i0.

              apply not_eq_sym.
              exact b.
    }

    (* supprimer la racine quand l’arbre est une feuille *)
    apply bt_eq.
      intros.
      inversion_clear H0.
      pose (H3 k H1).
      inversion i.
      subst.
      exfalso.
        apply H4.
        exact H1.

        exact H6.

        exact H6.

      intros.
      inversion H1.

    (* supprimer la racine quand l’arbre n’a qu’un fils gauche *)
    2: {
    apply bt_eq.
      intros.
      inversion_clear H0.
      elim (Nat.eq_dec n k).
        intros.
        subst.
        exfalso.
          apply H4.
          exact H1.

        intros.
        pose (H3 k H1).
        inversion i; subst.
          omega.

          exact H6.

          inversion H6.

      intros.
      inversion H; subst.
      inversion_clear H5.
      pose (H2 k H1).
      apply Nat.lt_neq in l.
      inversion H0; subst.
      apply H3.
        apply in_tree_left.
        exact H1.

        exact l.
    }

    (* supprimer la racine quand l’arbre n’a qu’un fils droit *)
    apply bt_eq.
      intros.
      inversion_clear H0.
      elim (Nat.eq_dec n k).
        intros.
        subst.
        exfalso.
          apply H4.
          exact H1.

        intros.
        pose (H3 k H1).
        inversion i; subst.
          omega.

          inversion H6.

          exact H6.

      intros.
      inversion H; subst.
      inversion_clear H6.
      pose (H2 k H1).
      apply Nat.lt_neq in g.
      inversion H0; subst.
      apply H3.
        apply in_tree_right.
        exact H1.

        apply not_eq_sym.
        exact g.

    (* cas impossible *)
    2: {
    apply bst_max_sound in e4.
      apply bt_max_exists in e4.
      exfalso.
      apply e4.
      reflexivity.

      inversion H.
      exact H6.
    }

    (* supprimer la racine quand l’arbre a deux sous-arbres *)
    inversion H.
    pose (bst_remove_sound xpred [_x0, _x1, _x2] (bst_remove xpred [_x0, _x1, _x2]) H6 eq_refl).
    inversion a.
    clear x tleft tright H4 H5 H6 H7 H1 H2 H3 a.
    apply bt_eq.
      intros.
      inversion H0.
      subst.
      pose (H3 k H1).
      inversion i; subst.
        exfalso.
          apply H4.
          exact H1.

        elim (Nat.eq_dec k xpred).
          intro; subst.
          apply in_tree_node.

          intro k_neq_xpred.
          apply in_tree_left.
          inversion H8; subst.
          apply H5.
            exact H7.

            exact k_neq_xpred.

        apply in_tree_right.
        exact H7.

      intros.
      apply bst_max_sound in e4.
        2: { inversion H; subst; apply H7. }

        inversion e4; subst.
        inversion H1; subst.
          inversion H0; subst.
          apply H2.
            apply in_tree_left.
            exact H3.

            inversion_clear H.
            inversion_clear H7.
            apply Nat.lt_neq.
            apply H.
            exact H3.

          inversion H0; subst.
            apply H2.
              apply in_tree_left.
              inversion H8; subst.
              apply H11.
              exact H6.

              inversion_clear H.
              inversion_clear H10.
              apply Nat.lt_neq.
              apply H.
              inversion H8; subst.
              apply H14.
              apply H6.

          inversion H0; subst.
          apply H2.
            apply in_tree_right.
            exact H6.

            inversion_clear H.
            inversion_clear H11.
            apply Nat.neq_sym.
            apply Nat.lt_neq.
            apply H.
            exact H6.
Qed.